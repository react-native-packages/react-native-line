# react-native-line

React Native for LINE login

## Installation

```sh
npm install react-native-line
```

## Usage

```js
import Line from "react-native-line";

// ...

const result = await Line.multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
