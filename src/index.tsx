import { NativeModules } from 'react-native';

type LineType = {
  multiply(a: number, b: number): Promise<number>;
};

const { Line } = NativeModules;

export default Line as LineType;
